from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base

from core.config.settings import settings
from core.connection.base_connection import BaseConnection

Base = declarative_base()


class DbConnection(BaseConnection):
    def __init__(self) -> None:
        self._db_url = settings.postgres.sqlalchemy_database_url()

    async def connect(self) -> None:
        async_engine = create_async_engine(self._db_url, echo=settings.postgres.echo, isolation_level="AUTOCOMMIT")
        async_session = AsyncSession(async_engine, expire_on_commit=False)
        self.db = async_session

    async def disconnect(self) -> None:
        await self.db.close()


db_conn = DbConnection()


def get_session() -> AsyncSession:
    return db_conn.db
