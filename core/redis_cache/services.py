from enum import Enum
from typing import Any

from core.redis_cache.enum import ExpirationTime
from core.redis_cache.keys import RedisKeys
from core.services.redis_services import redis_delete, redis_get, redis_set


class RedisCache:
    def __init__(self, enum_: Enum, user_id: int) -> None:
        self.enum_: Enum = enum_
        self.key = RedisKeys(self.enum_).tasks_key + f":{user_id}"

    async def get_value(self) -> Any:
        return await redis_get(key=self.key)

    async def set_value(self, value: Any, expiration_time: int | None) -> None:
        if expiration_time is None:
            expiration_time = getattr(ExpirationTime, self.enum_.name).value
        await redis_set(self.key, value, expiration_time)

    async def del_value(self) -> None:
        await redis_delete(self.key)
