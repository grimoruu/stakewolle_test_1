from datetime import timedelta
from enum import Enum


class RedisCacheKey(str, Enum):
    REFERRALS = "referrals:id"


class ExpirationTime(Enum):
    REFERRALS = timedelta(seconds=60 * 60)  # 60 min
