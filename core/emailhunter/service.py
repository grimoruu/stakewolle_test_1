import httpx

from core.config.settings import settings


async def verify_email(email: str) -> bool:
    url = settings.email_hunter.email_hunter_url() + f"&email={email}"
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        return response.status_code == 200
