from pydantic import BaseModel


class PostgresSettings(BaseModel):
    user: str
    password: str
    host: str
    port: int
    db_name: str
    echo: bool
    autoflush: bool

    def sqlalchemy_database_url(self) -> str:
        return f"postgresql+asyncpg://{self.user}:{self.password}@{self.host}:{self.port}/{self.db_name}"


class RedisSettings(BaseModel):
    host: str
    port: int

    def redis_url(self) -> str:
        return f"redis://{self.host}:{self.port}"


class EmailHunterSettings(BaseModel):
    api: str

    def email_hunter_url(self) -> str:
        return f"https://api.hunter.io/v2/email-verifier?api_key={self.api}"


class Settings(BaseModel):
    postgres: PostgresSettings
    redis: RedisSettings
    email_hunter: EmailHunterSettings
