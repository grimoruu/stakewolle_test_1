from typing import Any

from itsdangerous import BadSignature, URLSafeSerializer

from core.itsdangerous.enum import SecretKeys


class ItsDangerous:
    @staticmethod
    def create_token_data(**kwargs: Any) -> dict:
        return dict(**kwargs)

    @staticmethod
    def create_code(data: dict, key_name: str) -> str:
        secret_key = SecretKeys[key_name].value
        serializer = URLSafeSerializer(secret_key)
        return str(serializer.dumps(data))

    @staticmethod
    def deserializer(signed_token: str, key_name: str) -> dict:
        if signed_token:
            try:
                secret_key = SecretKeys[key_name].value
                serializer = URLSafeSerializer(secret_key)
                return serializer.loads(signed_token)
            except BadSignature:
                return {}
        return {}


its_dangerous_service = ItsDangerous()
