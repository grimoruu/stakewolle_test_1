from enum import Enum


class SecretKeys(Enum):
    REFERRALS_SECRET_KEY = "REFERRALS_SECRET_KEY"
