# Stakewolle

Проект Stakewolle - предоставляет пользователям возможность осуществлять CRUD операции (Создание, Чтение, Обновление, Удаление) с записями в базе данных. Стэк - Fastapi, Postgres, Alembic, SqlAlchemy, Poetry, Redis, Docker-compose, JWT. Emailhunter - для проверки указанного email адреса. Тестирование через Postman.

## Установка и Запуск

1. Клонирование репозитория:

```bash
git clone https://gitlab.com/grimoruu/stakewolle_test_1.git
```
2. Директория проекта:
```bash
cd stakewolle_test_1/
```
3. Установка зависимостей poetry:
```bash
poetry install
```
4. Миграция :
```bash
alembic revision --autogenerate -m "init"
```
```bash
alembic upgrade head
```
5. Сборка и запуск докеров:
```bash
docker compose build
```
```bash
docker compose up -d
```
6. Запуска приложения фастапи:
```bash
uvicorn main:app --reload
```
7. #### Веб-приложение будет доступно по адресу http://127.0.0.1:8000/docs

## CRUD

   Регистрация пользователя (POST запрос) http://127.0.0.1:8000/auth/signup/
   
```python
# Input через body
{
    "email": "test@mail.com",
    "password": "pass",
    "username": "pass",
    "referer_code": null  # может быть пустым или с реферральным кодом
}
```
```python
# Output
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTExODQzOTMsImlhdCI6MTcxMTAxMTU5Mywic2NvcGUiOiJhY2Nlc3NfdG9rZW4iLCJ1c2VyX2lkIjoxNH0.DaChcnSzACf9Ptx3BjfhtlVg2sw7N0GrFriooj-OKFg",
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTI3Mzk1OTMsImlhdCI6MTcxMTAxMTU5Mywic2NvcGUiOiJyZWZyZXNoX3Rva2VuIiwidXNlcl9pZCI6MTR9.q3ixLSSZPicTv8Nb6yLcasny2dheJWGOnnAqQ3DQExI"
}
```
   Логин пользователя (POST запрос) http://127.0.0.1:8000/auth/login/
   
```python
# Input через body
{
    "email": "test@mail.com",
    "password": "pass"
}
```
```python
# Output
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTExODQ0MTcsImlhdCI6MTcxMTAxMTYxNywic2NvcGUiOiJhY2Nlc3NfdG9rZW4iLCJ1c2VyX2lkIjoxNH0.OE3Ule_TkIJjvpXr5Zu3JGMr-T-P5IeWNf0w8nqcyKI",
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTI3Mzk2MTcsImlhdCI6MTcxMTAxMTYxNywic2NvcGUiOiJyZWZyZXNoX3Rva2VuIiwidXNlcl9pZCI6MTR9.uV2Tv5pgmtz_3dtn0630ZH54r79VTgOt1Dj2e-wn0DY"
}
```
Получение информации о рефералах пользователя (GET запрос)

http://127.0.0.1:8000/users/user/{user_id}

либо

http://127.0.0.1:8000/users/user/ - если пользователь уже зарегался или залогинился, айди юзера берется из аксес токена, проверка через депендс

акссес токен передавать через хэдр
Key - authorization, Value - Bearer {access token}


```python
# Output
{
    "id": 1,
    "referrals": [
        {
            "id": 2,
            "username": "pass",
            "email": "email1@mail.com"
        },
        {
            "id": 4,
            "username": "pass",
            "email": "email2@mail.com"
        },
    ]
}
```
Создание реферального кода (POST запрос)

реферальный код представляет собой itsdangerous token, хранит в себе айди и почту реферера. Код будет кешироваться в редисе, с заданным exp time.

http://127.0.0.1:8000/users/user/code/set_referral_code - так же айди через депендс, для создания, получения и удаления реферального кода юзер должен быть авторизован

```python
# Input через body
{
    "expiration_time": null  # можно задавать время в секундах или оставить пустым (по умолчанию будет браться 3600 сек)
}
```
```python
# Output
{
    "referral_code": "eyJlbWFpbCI6ImVtYWlsQG1haWwuY29tIiwidXNlcl9pZCI6MX0.pYCpZqCypAtgkgPIn0CP4EG0wKc"
}
```
Получение реферального кода (GET запрос)

http://127.0.0.1:8000/users/user/code/get_referral_code

```python
# Output
{
    "referral_code": "eyJlbWFpbCI6ImVtYWlsQG1haWwuY29tIiwidXNlcl9pZCI6MX0.pYCpZqCypAtgkgPIn0CP4EG0wKc"
}
```
Удаление реферального кода (DELETE запрос)

http://127.0.0.1:8000/users/user/code/delete_referral_code