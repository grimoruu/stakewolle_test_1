from sqlalchemy import exists, insert, select
from sqlalchemy.engine import Row

from core.services.db_service import BaseDAO
from db.models import User


class AuthServiceDAO(BaseDAO):
    async def check_users_exist(self, email: str) -> bool:
        query = exists(select(User.id).where(User.email == email)).select()
        result = await self._db.execute(query)
        return result.scalar_one()

    async def add_new_user(self, username: str, hashed_password: str, email: str, referer_email: str | None) -> int:
        query = (
            insert(User)
            .values(username=username, hashed_password=hashed_password, email=email, referer_email=referer_email)
            .returning(User.id)
        )
        result = await self._db.execute(query)
        return result.scalar_one()

    async def login_user(self, email: str) -> Row | None:
        select_query = (
            select(
                User.id,
                User.hashed_password,
            )
            .select_from(User)
            .where(User.email == email)
        )
        result = await self._db.execute(select_query)
        return result.fetchone()
