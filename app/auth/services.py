from fastapi import Depends, HTTPException, status
from itsdangerous import BadSignature

from app.auth.dao import AuthServiceDAO
from app.auth.schemas import (
    CreateUserSchema,
    JWTResponse,
    LoginUserSchema,
    RefreshTokenSchema,
)
from core.auth_utils.auth import auth_utils
from core.emailhunter.service import verify_email
from core.itsdangerous.enum import SecretKeys
from core.itsdangerous.service import its_dangerous_service
from core.redis_cache.enum import RedisCacheKey
from core.redis_cache.services import RedisCache


class AuthService:
    def __init__(self, auth_dao: AuthServiceDAO = Depends(AuthServiceDAO)) -> None:
        self._auth_dao = auth_dao

    async def create_user_services(self, payload: CreateUserSchema) -> JWTResponse:
        is_valid_email = await verify_email(payload.email)
        if not is_valid_email:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Invalid email address",
            )
        if await self._auth_dao.check_users_exist(email=payload.email):
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Account already exist")

        hashed_password = auth_utils.encode_password(password=payload.password)
        referer_email = None

        if payload.referer_code:
            try:
                data = its_dangerous_service.deserializer(
                    signed_token=payload.referer_code, key_name=SecretKeys.REFERRALS_SECRET_KEY.name
                )
            except BadSignature:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Token signature verification error",
                )

            redis_cache = RedisCache(enum_=RedisCacheKey.REFERRALS, user_id=data["user_id"])
            referer_email = data["email"] if await redis_cache.get_value() else None

        user_id = await self._auth_dao.add_new_user(
            username=payload.username, hashed_password=hashed_password, email=payload.email, referer_email=referer_email
        )
        tokens = auth_utils.create_jwt_tokens(user_id=user_id)
        return JWTResponse(access_token=tokens.access_token, refresh_token=tokens.refresh_token)

    async def login_user_services(self, payload: LoginUserSchema) -> JWTResponse:
        user = await self._auth_dao.login_user(email=payload.email)
        if not user:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Account dont exist or Incorrect Email",
            )
        if not auth_utils.verify_password(password=payload.password, encoded_password=user.hashed_password):
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Incorrect Password")
        tokens = auth_utils.create_jwt_tokens(user_id=user.id)
        return JWTResponse(access_token=tokens.access_token, refresh_token=tokens.refresh_token)

    @staticmethod
    def refresh_access_token_services(payload: RefreshTokenSchema) -> JWTResponse:
        tokens = auth_utils.create_jwt_tokens(user_id=auth_utils.decode_refresh_token(token=payload.refresh_token))
        return JWTResponse(access_token=tokens.access_token, refresh_token=tokens.refresh_token)
