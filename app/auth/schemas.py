from pydantic import BaseModel, EmailStr


class UserSchemaBase(BaseModel):
    email: EmailStr
    password: str


class CreateUserSchema(UserSchemaBase):
    username: str
    referer_code: str | None


class LoginUserSchema(UserSchemaBase):
    pass


class JWTResponse(BaseModel):
    access_token: str
    refresh_token: str


class RefreshTokenSchema(BaseModel):
    refresh_token: str
