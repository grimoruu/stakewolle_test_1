from fastapi import Depends

from app.users.dao import UserServiceDAO
from app.users.schemas import ReferralCodeSchemaResponse, ReferralWithTimeSchema, UserSchema, UsersSchemaResponse
from core.itsdangerous.enum import SecretKeys
from core.itsdangerous.service import its_dangerous_service
from core.redis_cache.enum import RedisCacheKey
from core.redis_cache.services import RedisCache


class UserService:
    def __init__(self, user_dao: UserServiceDAO = Depends(UserServiceDAO)) -> None:
        self._user_dao = user_dao

    async def get_user_service(self, user_id: int) -> UsersSchemaResponse:
        items = await self._user_dao.get_referrals(user_id)
        result = UsersSchemaResponse(
            id=user_id,
            referrals=[
                UserSchema(
                    id=row.id,
                    username=row.username,
                    email=row.email,
                )
                for row in items
            ],
        )
        return result

    async def set_referral_code_services(
        self, user_id: int, expiration_time: ReferralWithTimeSchema
    ) -> ReferralCodeSchemaResponse:
        email = await self._user_dao.get_user_email(user_id)

        services_name = SecretKeys.REFERRALS_SECRET_KEY
        referral_code = its_dangerous_service.create_code(
            data={"email": email, "user_id": user_id}, key_name=services_name.name
        )
        redis_cache = RedisCache(enum_=RedisCacheKey.REFERRALS, user_id=user_id)
        await redis_cache.set_value(value=referral_code, expiration_time=expiration_time.expiration_time)
        return ReferralCodeSchemaResponse(referral_code=referral_code)

    @staticmethod
    async def get_referral_code_services(user_id: int) -> ReferralCodeSchemaResponse | None:
        redis_cache = RedisCache(enum_=RedisCacheKey.REFERRALS, user_id=user_id)
        referral_code = await redis_cache.get_value()
        return ReferralCodeSchemaResponse(referral_code=referral_code) if referral_code else None

    @staticmethod
    async def delete_referral_code_services(user_id: int) -> None:
        redis_cache = RedisCache(enum_=RedisCacheKey.REFERRALS, user_id=user_id)
        await redis_cache.del_value()
