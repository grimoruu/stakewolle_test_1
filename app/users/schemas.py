from pydantic import BaseModel


class UserSchema(BaseModel):
    id: int
    username: str
    email: str


class UsersSchemaResponse(BaseModel):
    id: int
    referrals: list[UserSchema]


class ReferralCodeSchemaResponse(BaseModel):
    referral_code: str


class ReferralWithTimeSchema(BaseModel):
    expiration_time: int | None
