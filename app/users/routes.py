from fastapi import APIRouter, Depends

from app.users.depends import get_user_id
from app.users.schemas import ReferralCodeSchemaResponse, ReferralWithTimeSchema, UsersSchemaResponse
from app.users.services import UserService

router = APIRouter()


@router.get("/user/{user_id}")
async def get_referrals_by_user_id(user_id: int, service: UserService = Depends(UserService)) -> UsersSchemaResponse:
    return await service.get_user_service(user_id)


@router.get("/user")
async def get_referrals_by_user(
    user_id: int = Depends(get_user_id), service: UserService = Depends(UserService)
) -> UsersSchemaResponse:
    return await service.get_user_service(user_id)


@router.post("/user/code/set_referral_code")
async def set_referral_code(
    expiration_time: ReferralWithTimeSchema,
    user_id: int = Depends(get_user_id),
    service: UserService = Depends(UserService),
) -> ReferralCodeSchemaResponse:
    return await service.set_referral_code_services(user_id, expiration_time)


@router.get("/user/code/get_referral_code")
async def get_referrals_code(
    user_id: int = Depends(get_user_id), service: UserService = Depends(UserService)
) -> ReferralCodeSchemaResponse | None:
    return await service.get_referral_code_services(user_id)


@router.delete("/user/code/delete_referral_code")
async def del_referral_code(user_id: int = Depends(get_user_id), service: UserService = Depends(UserService)) -> None:
    await service.delete_referral_code_services(user_id)
