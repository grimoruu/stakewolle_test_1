from sqlalchemy import alias, select

from core.services.db_service import BaseDAO
from db.models import User


class UserServiceDAO(BaseDAO):

    async def get_referrals(self, user_id: int) -> list:
        user = alias(User)
        referral = alias(User)
        query = (
            select(referral.c.id, referral.c.username, referral.c.email)
            .select_from(user.join(referral, referral.c.referer_email == user.c.email))
            .where(user.c.id == user_id)
        )
        result = await self._db.execute(query)
        return result.fetchmany()

    async def get_user_email(self, user_id: int) -> str | None:
        query = select(User.email).select_from(User).where(User.id == user_id)
        result = await self._db.execute(query)
        return result.scalar_one()
