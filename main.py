from fastapi import FastAPI

from app.routes import router
from core.connection.db_connection import db_conn
from core.connection.redis_connection import redis_conn

app = FastAPI()
services = [db_conn, redis_conn]


@app.on_event("startup")
async def initialize_connections() -> None:
    for service in services:
        await service.connect()


@app.on_event("shutdown")
async def close_connection() -> None:
    for service in services:
        await service.disconnect()


app.include_router(router)
