from sqlalchemy import Column, Integer, String

from core.connection.db_connection import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False)
    hashed_password = Column(String, nullable=False)
    email = Column(String, nullable=False, unique=True)
    referral_code = Column(String, nullable=True, unique=True)
    referer_email = Column(String, nullable=True, unique=False)
